import random as rd
import numpy as np
import matplotlib.pyplot as plt
from skimage import io
import cv2

def greys():

  image = io.imread('pepper.bmp')
  grey=[]
  for i in range(len(image)):
      grey.append([])
      for j in range(len(image[0])):
          g = int(0.3 * image[i,j,0] + 0.3 * image[i,j,1] + 0.3 * image[i,j,2])
          g = max(0, g)
          g = min(g, 255)
          grey[i].append(g)
  return np.array(grey)

def greens(image):

    green=[]
    for i in range(len(image)):
        green.append([])
        for j in range(len(image[0])):
            green[i].append(image[i,j,1])
    return np.array(green)

def reds(image):

    red=[]
    for i in range(len(image)):
        red.append([])
        for j in range(len(image[0])):
            red[i].append(image[i,j,0])
    return np.array(red)

def blues(image):

    blue=[]
    for i in range(len(image)):
        blue.append([])
        for j in range(len(image[0])):
            blue[i].append(image[i,j,2])
    return np.array(blue)

def toRGB(imR,imG,imB):
    output=[]
    for i in range(len(imR)):
        output.append([])
        for j in range(len(imR[0])):
            output[i].append([round(imR[i,j]),round(imG[i,j]),round(imB[i,j]),255])
    return np.array(output)

def seuillage(image):
  s = rd.randint(0,256)
  old_s = -1
  while s != old_s:
    d = {i:0 for i in range(256)}
    for i in image:
      for j in i:
        d[j] += 1
    s1=0
    s2=0
    for key in d.keys():
        if key < s:
            s1 += key*d[key]
        else:
            s2 += key*d[key]
    s1 = s1/max(1,sum(list(d.values())[:int(s)]))
    s2 = s2/max(1,sum(list(d.values())[int(s):]))
    old_s = s
    s = (s1+s2)/2
  return s,s1,s2

def kmeans(k, image):
    l = sorted(rd.randint(0,256) for i in range(k))
    d = {l[i] : [] for i in range(k)}
    old_d = {-1:[] for i in d.keys()}
    while list(d.keys()) != list(old_d.keys()):
        for i in image:
            for j in i:
                d[min(d, key = lambda x : abs(j-x))].append(j)
        old_d = d
        d = {sum(d[i])/max(1,len(d[i])):[] for i in d.keys()}
        print(list(old_d.keys()), list(d.keys()))
    return d

def classImage(classes,image):
    output = []
    for i in range(len(image)):
        output.append([])
        for j in range(len(image[0])):
            output[i].append(min(classes,key = lambda x : abs(image[i,j]-x)))
    return np.array(output)

def seuillageClass():
    img = cv2.imread("pepper.bmp")
    image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    s,s1,s2 = seuillage(image)

    output_1 = []
    output_2 = []
    for i in range(len(image)):
        output_1.append([])
        output_2.append([])
        for j in range(len(image[0])):
            output_1[i].append([255,0][image[i,j] < s])
            output_2[i].append([s2,s1][image[i,j] < s])
    output_1 = np.array(output_1)
    output_2 = np.array(output_2)

    fig, axs = plt.subplots(1,3, figsize=(13,10), squeeze=False)

    axs[0][0].set_title("Original Image")
    axs[0][0].imshow(image, cmap=plt.cm.gray, vmin=0, vmax=255)
    axs[0][1].set_title("Proposition 1")
    axs[0][1].imshow(output_1, cmap=plt.cm.gray, vmin=0, vmax=255)
    axs[0][2].set_title("Proposition 2")
    axs[0][2].imshow(output_2, cmap=plt.cm.gray, vmin=0, vmax=255)

    plt.show()

def kmeansClass():
    img = cv2.imread("pepper.bmp")
    image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    cl2 = kmeans(2, image).keys()
    img_2 = classImage(cl2,image)

    cl4 = kmeans(4, image).keys()
    img_4 = classImage(cl4,image)

    cl8 = kmeans(8, image).keys()
    img_8 = classImage(cl8,image)

    # displays images and histograms
    fig, axs = plt.subplots(2,2, figsize=(13,10))
    axs[0][0].set_title("Original Image")
    axs[0][0].imshow(image, cmap=plt.cm.gray, vmin=0, vmax=255)
    axs[0][1].set_title("Image with K = 2")
    axs[0][1].imshow(img_2, cmap=plt.cm.gray, vmin=0, vmax=255)
    axs[1][0].set_title("Image with K = 4")
    axs[1][0].imshow(img_4, cmap=plt.cm.gray, vmin=0, vmax=255)
    axs[1][1].set_title("Image with K = 8")
    axs[1][1].imshow(img_8, cmap=plt.cm.gray, vmin=0, vmax=255)

    plt.show()

def comb():
    img_c = cv2.imread("CT_COVID.png")
    img_nc = cv2.imread("CT_NonCOVID.jpg")

    img_c = cv2.cvtColor(img_c, cv2.COLOR_BGR2GRAY)
    img_nc = cv2.cvtColor(img_nc, cv2.COLOR_BGR2GRAY)

    # Seuillage

    s_c,s1_c,s2_c = seuillage(img_c)
    s_nc,s1_nc,s2_nc = seuillage(img_nc)

    output_1 = []
    for i in range(len(img_c)):
        output_1.append([])
        for j in range(len(img_c[0])):
            output_1[i].append([s2_c,s1_c][img_c[i,j] < s_c])
    output_1 = np.array(output_1)

    output_2 = []
    for i in range(len(img_nc)):
        output_2.append([])
        for j in range(len(img_nc[0])):
            output_2[i].append([s2_nc,s1_nc][img_nc[i,j] < s_nc])
    output_2 = np.array(output_2)

    # KMeans

    cl7_c = kmeans(5,img_c).keys()
    img_kc = classImage(cl7_c,img_c)
    cl7_nc = kmeans(5,img_nc).keys()
    img_knc = classImage(cl7_nc,img_nc)

    # Affichage

    fig, axs = plt.subplots(2,3, figsize=(13,10))
    axs[0][0].set_title("Poumon affecté")
    axs[0][0].imshow(img_c, cmap=plt.cm.gray, vmin=0, vmax=255)
    axs[0][1].set_title("Seuillage")
    axs[0][1].imshow(output_1, cmap=plt.cm.gray, vmin=0, vmax=255)
    axs[0][2].set_title("KMeans (K=5)")
    axs[0][2].imshow(img_kc, cmap=plt.cm.gray, vmin=0, vmax=255)
    axs[1][0].set_title("Poumon non-affecté")
    axs[1][0].imshow(img_nc, cmap=plt.cm.gray, vmin=0, vmax=255)
    axs[1][1].set_title("Seuillage")
    axs[1][1].imshow(output_2, cmap=plt.cm.gray, vmin=0, vmax=255)
    axs[1][2].set_title("KMeans (K=5)")
    axs[1][2].imshow(img_knc, cmap=plt.cm.gray, vmin=0, vmax=255)

    plt.show()

def kmeansColor():
    image = io.imread('perroquet_couleur.png')
    blouje = blues(image)
    rouge = reds(image)
    vouge = greens(image)

    # KMeans

    r4 = kmeans(4,rouge).keys()
    img_r4 = classImage(r4,rouge)
    b4 = kmeans(4,blouje).keys()
    img_b4 = classImage(b4,blouje)
    g4 = kmeans(4,vouge).keys()
    img_g4 = classImage(g4,vouge)
    img_4 = toRGB(img_r4,img_b4,img_g4)

    r8 = kmeans(8,rouge).keys()
    img_r8 =  classImage(r8,rouge)
    b8 = kmeans(8,blouje).keys()
    img_b8 = classImage(b8,blouje)
    g8 = kmeans(8,vouge).keys()
    img_g8 = classImage(g8,vouge)
    img_8 = toRGB(img_r8,img_g8,img_b8)    

    r32 = kmeans(32,rouge).keys()
    img_r32 = classImage(r32,rouge)    
    b32 = kmeans(32,blouje).keys()
    img_b32 = classImage(b32,blouje)
    g32 = kmeans(32,vouge).keys()
    img_g32 = classImage(g32,vouge)
    img_32 = toRGB(img_r32,img_g32,img_b32)

    # Affichage

    
    fig, axs = plt.subplots(1,4, figsize=(13,10), squeeze=False)
    axs[0][0].set_title("Image originale")
    axs[0][0].imshow(image)
    axs[0][1].set_title("K = 4")
    axs[0][1].imshow(img_4)
    axs[0][2].set_title("K = 8")
    axs[0][2].imshow(img_8)
    axs[0][3].set_title("K = 32")
    axs[0][3].imshow(img_32)

    plt.show()

if __name__=="__main__":
    #seuillageClass()
    #kmeansClass()
    #comb()
    kmeansColor()